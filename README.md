# Use this boilerplate

1. Download [zip from GitLab](https://gitlab.com/skilloverse/static-vanilla-webpack-website/-/archive/master/static-vanilla-webpack-website-master.zip), extract, and enter the directory.
1. Change `package.json` details to suit your project.
1. (Optional) Run `git init` to initialise a git repository for your project.
1. Code HTML using Pug in `src/index.pug`; CSS using SCSS in `src/index.scss`; and JavaScript using TypeScript in `src/index.ts`.
1. Run a development server with `yarn run start:dev`
1. Build for production with `yarn build`
1. After building, the files can be found in the `dist` directory.
