// After installing, import any style and script libraries here.
// E.g. import 'bootstrap'
import './index.scss'

// Delete this test Typescript code and write app code here.
const intro: HTMLElement = document.getElementById('introduction')
const currentText: string = intro.textContent
intro.textContent = `${currentText}, World!`