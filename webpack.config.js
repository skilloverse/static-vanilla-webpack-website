const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	entry: './src/index.ts',
	plugins: [
		new HtmlWebpackPlugin({ template: './src/index.pug' }),
		new HtmlWebpackPlugin({
			filename: 'example-page.html',
			template: './src/example-page.pug',
			chunks: ['example-page'],
		}),
	],
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'ts-loader',
			},
			{
				test: /\.pug$/,
				use: 'pug-loader',
			},
			{
				test: /\.s?css$/i,
				use: ['style-loader', 'css-loader', 'sass-loader'],
			},
		],
	},
	entry: {
		'index': './src/index.ts',
		'example-page': './src/example-page.ts',
	},
	resolve: {
		extensions: ['.ts', '.pug', '.js', '.scss', '.css']
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
	},
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
	}
}
